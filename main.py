from flask import Flask, request, make_response, redirect, render_template, session

app = Flask(__name__)

app.config['SECRET_KEY'] = 'masystech'

@app.route('/')
def index():
    user_ip = request.remote_addr
    response = make_response(redirect ('/hello'))
    session['user_ip'] = user_ip

    return response

@app.route('/hello') 
def hello():
    user_ip = session.get('user_ip')

    return render_template('hello.html', user_ip=user_ip)
 